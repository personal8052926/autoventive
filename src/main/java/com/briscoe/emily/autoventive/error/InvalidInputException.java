package com.briscoe.emily.autoventive.error;

public class InvalidInputException extends RuntimeException {

    public InvalidInputException(final String message) {
        super(message);
    }

}
