package com.briscoe.emily.autoventive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutoventiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutoventiveApplication.class, args);
	}

}
