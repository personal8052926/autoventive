package com.briscoe.emily.autoventive.vehicles.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Entity
@Table(name = "vehicle")
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(toBuilder = true)
@Data
public class Vehicle {

    @Id
    @NotEmpty(message = "Invalid VIN: A VIN must be provided")
    @JsonProperty("VIN")
    private String vin;

    @NotNull(message = "Invalid Manufacturer: A manufacturer must be provided")
    private Manufacturer manufacturer;

    // Assuming that bayNumber must be positive
    @Min(value = 1, message = "Invalid Bay Number: bayNumber must be empty, or greater than 0")
    private Integer bayNumber;
}