package com.briscoe.emily.autoventive.vehicles;

import com.briscoe.emily.autoventive.vehicles.dto.CreateVehiclesRequestDto;
import com.briscoe.emily.autoventive.vehicles.model.Vehicle;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class VehicleController {

    private final VehicleService vehicleService;

    @PostMapping("/vehicles")
    public List<Vehicle> createVehicles(@Valid @RequestBody final CreateVehiclesRequestDto createVehiclesRequestDto){
        // Return the saved vehicles in case any future additions to the endpoint apply any processing
        // that may lead to the saved objects differing from the request
        return vehicleService.saveVehicles(createVehiclesRequestDto.vehicles());
    }
    
}
