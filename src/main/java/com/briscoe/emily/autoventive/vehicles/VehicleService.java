package com.briscoe.emily.autoventive.vehicles;

import com.briscoe.emily.autoventive.error.InvalidInputException;
import com.briscoe.emily.autoventive.vehicles.model.Manufacturer;
import com.briscoe.emily.autoventive.vehicles.model.Vehicle;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@AllArgsConstructor
class VehicleService {

    private final VehicleRepository vehicleRepository;

    public List<Vehicle> saveVehicles(final List<Vehicle> newVehicles) {
        final List<Vehicle> vehiclesToSave = removeVehiclesNotToBeSaved(newVehicles);
        if (vehiclesToSave.isEmpty()) {
            // Assuming it's valid to provide a list of vehicles that don't get saved
            return Collections.emptyList();
        }
        final List<String> vinsOfVehiclesToSave = vehiclesToSave.stream().map(Vehicle::getVin).toList();
        if (doesListHaveDuplicates(vinsOfVehiclesToSave)) {
            // Assuming that you cannot save a duplicate VIN
            throw new InvalidInputException("You cannot save two vehicles with the same VIN");
        }
        final boolean anyVinsAlreadyExist = vehicleRepository.existsByVinIn(vinsOfVehiclesToSave);
        if (anyVinsAlreadyExist) {
            // Assuming that you cannot save a duplicate VIN
            throw new InvalidInputException("One of the VINs provided already exists");
        }
        return vehicleRepository.saveAll(vehiclesToSave);
    }

    private List<Vehicle> removeVehiclesNotToBeSaved(final List<Vehicle> newVehicles) {
        return newVehicles
                .stream()
                // Assuming that there will be more manufacturers on the white-list than black-list
                .filter(vehicle -> !Manufacturer.VW.equals(vehicle.getManufacturer()))
                .filter(vehicle -> {
                    if (vehicle.getBayNumber() == null) {
                        return true;
                    }
                    return vehicle.getBayNumber() <= 100;
                })
                .toList();
    }

    private boolean doesListHaveDuplicates(final List<?> listToCheck) {
        final Set<?> setOfListItems = new HashSet<>(listToCheck);
        return setOfListItems.size() < listToCheck.size();
    }

}
