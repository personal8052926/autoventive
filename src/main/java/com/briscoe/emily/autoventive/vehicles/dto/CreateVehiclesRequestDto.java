package com.briscoe.emily.autoventive.vehicles.dto;

import com.briscoe.emily.autoventive.vehicles.model.Vehicle;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;

import java.util.List;

public record CreateVehiclesRequestDto(
        @Valid @NotEmpty(message = "You must provide at least one vehicle to save") List<Vehicle> vehicles) {
}
