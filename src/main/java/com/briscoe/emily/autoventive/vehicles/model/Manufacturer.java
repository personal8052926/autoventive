package com.briscoe.emily.autoventive.vehicles.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Manufacturer {
    @JsonProperty("Ford")
    FORD,
    GM,
    VW,
}
