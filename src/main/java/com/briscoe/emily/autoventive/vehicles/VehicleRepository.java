package com.briscoe.emily.autoventive.vehicles;

import com.briscoe.emily.autoventive.vehicles.model.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
interface VehicleRepository extends JpaRepository<Vehicle, String> {

    boolean existsByVinIn(List<String> vin);

}
