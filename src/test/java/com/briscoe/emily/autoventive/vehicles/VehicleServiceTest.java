package com.briscoe.emily.autoventive.vehicles;

import com.briscoe.emily.autoventive.error.InvalidInputException;
import com.briscoe.emily.autoventive.vehicles.model.Manufacturer;
import com.briscoe.emily.autoventive.vehicles.model.Vehicle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class VehicleServiceTest {

    private VehicleRepository mockVehicleRepository;

    private VehicleService classUnderTest;

    private String validGmVIN;
    private Vehicle validGm;

    @BeforeEach
    void setup() {
        mockVehicleRepository = mock(VehicleRepository.class);

        classUnderTest = new VehicleService(mockVehicleRepository);

        validGmVIN = "Valid VIN";
        validGm = new Vehicle(validGmVIN, Manufacturer.GM, 20);
    }

    @Test
    void saveVehicleDoesntSaveVw() {
        final Vehicle vw = validGm.toBuilder().manufacturer(Manufacturer.VW).build();

        final List<Vehicle> savedVehicles = classUnderTest.saveVehicles(List.of(vw));

        assertEquals(0, savedVehicles.size());
        verify(mockVehicleRepository, never()).existsByVinIn(any());
        verify(mockVehicleRepository, never()).saveAll(any());
    }

    @ParameterizedTest
    @EnumSource(value = Manufacturer.class, names = {"GM", "FORD"})
    void saveVehiclesSavesOneVehicleAndReturnsSavedRecord(Manufacturer manufacturer) {
        final Vehicle vehicleWithManufacturer = validGm.toBuilder().manufacturer(manufacturer).build();
        when(mockVehicleRepository.existsByVinIn(List.of(validGmVIN))).thenReturn(false);
        when(mockVehicleRepository.saveAll(List.of(vehicleWithManufacturer))).thenReturn(List.of(vehicleWithManufacturer));

        final List<Vehicle> savedVehicles = classUnderTest.saveVehicles(List.of(vehicleWithManufacturer));

        assertEquals(1, savedVehicles.size());
        assertEquals(vehicleWithManufacturer, savedVehicles.getFirst());

        verify(mockVehicleRepository, times(1)).saveAll(List.of(vehicleWithManufacturer));
    }

    @Test
    void saveVehiclesSavesOneGMWithNoParkingBayAndReturnsSavedRecord() {
        final Vehicle gMWithNoParkingBay = validGm.toBuilder().bayNumber(null).build();
        when(mockVehicleRepository.existsByVinIn(List.of(validGmVIN))).thenReturn(false);
        when(mockVehicleRepository.saveAll(List.of(gMWithNoParkingBay))).thenReturn(List.of(gMWithNoParkingBay));

        final List<Vehicle> savedVehicles = classUnderTest.saveVehicles(List.of(gMWithNoParkingBay));

        assertEquals(1, savedVehicles.size());
        assertEquals(gMWithNoParkingBay, savedVehicles.getFirst());

        verify(mockVehicleRepository, times(1)).saveAll(List.of(gMWithNoParkingBay));
    }

    @Test
    void saveVehiclesSavesOneGMButNotVW() {
        final Vehicle vW = new Vehicle("VW VIN", Manufacturer.VW, 21);

        when(mockVehicleRepository.existsByVinIn(List.of(validGmVIN))).thenReturn(false);
        when(mockVehicleRepository.saveAll(List.of(validGm))).thenReturn(List.of(validGm));

        final List<Vehicle> savedVehicles = classUnderTest.saveVehicles(List.of(validGm, vW));

        assertEquals(1, savedVehicles.size());
        assertEquals(validGm, savedVehicles.getFirst());

        verify(mockVehicleRepository, times(1)).saveAll(List.of(validGm));
    }

    @ParameterizedTest
    @ValueSource(ints = {101, 102, 1000, 30000, 3512354})
    void saveVehiclesSavesOneGMIfSecondIsParkedInSpotOver100(int secondBayNumber) {
        final Vehicle gMParkedOver100 = new Vehicle("Over Parked VIN", Manufacturer.GM, secondBayNumber);

        when(mockVehicleRepository.existsByVinIn(List.of(validGmVIN))).thenReturn(false);
        when(mockVehicleRepository.saveAll(List.of(validGm))).thenReturn(List.of(validGm));

        final List<Vehicle> savedVehicles = classUnderTest.saveVehicles(List.of(validGm, gMParkedOver100));

        assertEquals(1, savedVehicles.size());
        assertEquals(validGm, savedVehicles.getFirst());

        verify(mockVehicleRepository, times(1)).saveAll(List.of(validGm));
    }

    @ParameterizedTest
    @ValueSource(ints = {100, 2, 30, 45, 99})
    void saveVehiclesSavesBothGMIfSecondIsParkedInSpot100OrLess(int secondBayNumber) {
        final Vehicle gmParkedAt100OrLess = new Vehicle("Under Parked VIN", Manufacturer.GM, secondBayNumber);

        when(mockVehicleRepository.existsByVinIn(List.of(validGmVIN, gmParkedAt100OrLess.getVin()))).thenReturn(false);
        when(mockVehicleRepository.saveAll(List.of(validGm, gmParkedAt100OrLess))).thenReturn(List.of(validGm, gmParkedAt100OrLess));

        final List<Vehicle> savedVehicles = classUnderTest.saveVehicles(List.of(validGm, gmParkedAt100OrLess));

        assertEquals(2, savedVehicles.size());
        assertEquals(validGm, savedVehicles.get(0));
        assertEquals(gmParkedAt100OrLess, savedVehicles.get(1));

        verify(mockVehicleRepository, times(1)).saveAll(List.of(validGm, gmParkedAt100OrLess));
    }

    @Test
    void saveVehiclesThrowsInvalidInputExceptionWhenTwoVehiclesHaveTheSameVin() {
        final InvalidInputException e = assertThrows(InvalidInputException.class, () -> {
            classUnderTest.saveVehicles(List.of(validGm, validGm));
        });
        assertEquals("You cannot save two vehicles with the same VIN", e.getMessage());
    }

    @Test
    void saveVehiclesThrowsInvalidInputExceptionWhenVehicleAlreadyInDatabase() {
        when(mockVehicleRepository.existsByVinIn(List.of(validGmVIN))).thenReturn(true);

        final InvalidInputException e = assertThrows(InvalidInputException.class, () -> {
            classUnderTest.saveVehicles(List.of(validGm));
        });
        assertEquals("One of the VINs provided already exists", e.getMessage());
    }

}
