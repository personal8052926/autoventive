package com.briscoe.emily.autoventive.vehicles;

import com.briscoe.emily.autoventive.vehicles.dto.CreateVehiclesRequestDto;
import com.briscoe.emily.autoventive.vehicles.model.Manufacturer;
import com.briscoe.emily.autoventive.vehicles.model.Vehicle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class VehicleControllerTest {

    private VehicleService mockVehicleService;

    private VehicleController classUnderTest;

    @BeforeEach
    void setup() {
        mockVehicleService = mock(VehicleService.class);
        classUnderTest = new VehicleController(mockVehicleService);
    }

    @Test
    void createVehiclesPassesCorrectArgumentToVehicleServiceAndGivesResponse() {
        final Vehicle vehicle1 = new Vehicle("TEST_VIN", Manufacturer.FORD, 32);
        final Vehicle vehicle2 = new Vehicle("TEST_VIN_SAVED", Manufacturer.FORD, 32);
        // When saving a vehicle, return a vehicle with a different VIN so we can verify the save response is whats
        // returned to the user, and we aren't accidentally returning the argument passed in.
        when(mockVehicleService.saveVehicles(List.of(vehicle1))).thenReturn(List.of(vehicle2));

        final List<Vehicle> savedVehicles = classUnderTest.createVehicles(new CreateVehiclesRequestDto(List.of(vehicle1)));

        assertEquals(1, savedVehicles.size());
        assertEquals(vehicle2.getVin(), savedVehicles.getFirst().getVin());
    }

}
